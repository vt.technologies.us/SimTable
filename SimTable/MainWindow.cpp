#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QMessageBox>
#include <wiringPi.h>
#include <QThread>
#include <QDebug>
#include <QString>
#include <QTextCodec>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	initUI();
	wiringPiSetup();
	pinMode(0, OUTPUT);
	
	QThread *controllerThread = new QThread; 
	QThread *timerThread = new QThread; 
	QThread *serialThread = new QThread;
	//QThread *UIThread = new QThread;
	
	mySerial =  new Serial(); 
	Ctrl = new Controller();
	timer = new QTimer;
	timer->setInterval(1000);

	mySerial->moveToThread(serialThread);
	mySerial->thisPort->moveToThread(serialThread); 		//  the inner field thisPort->moveToTheThread(...) does the magic !!!
	timer->moveToThread(timerThread);
	Ctrl->moveToThread(controllerThread);
	
	connect(serialThread, SIGNAL(started()), mySerial, SLOT(PortProcess())); 
	connect(mySerial, SIGNAL(DataReadyRead(QString)), this, SLOT(serialDataReceived(QString))); 
	connect(mySerial, SIGNAL(PortError(QString)), this, SLOT(printError(QString)));
	connect(timerThread, SIGNAL(started()), timer, SLOT(start()));
	connect(controllerThread, SIGNAL(started()), this, SLOT(ButtonClicked()));
	connect(timer, SIGNAL(timeout()), Ctrl, SLOT(update()));
	qRegisterMetaType<QVector<double>>("QVector<double>");
	connect(Ctrl, SIGNAL(controlInfo(QVector<double>)), this, SLOT(updateUI(QVector<double>)));
	connect(Ctrl, SIGNAL(controlInfo(QVector<double>)), this, SLOT(updateSerial(QVector<double>)));
	connect(controllerThread, &QThread::finished, controllerThread, &QThread::deleteLater); 
	
	connect(mySerial, SIGNAL(FinishedPort()), serialThread, SLOT(quit())); 
	connect(serialThread, &QThread::finished, mySerial, &Serial::deleteLater);
	connect(serialThread, &QThread::finished, serialThread, &QThread::deleteLater);
	connect(timerThread, &QThread::finished, timer, &QTimer::deleteLater);
	connect(timerThread, &QThread::finished, timerThread, &QThread::deleteLater);
	connect(controllerThread, &QThread::finished, Ctrl, &Controller::deleteLater);
	connect(controllerThread, &QThread::finished, controllerThread, &QThread::deleteLater); 
	
	mySerial->initPort("/dev/ttyAMA0", QSerialPort::Baud115200, QSerialPort::Data8, 
						QSerialPort::NoParity, QSerialPort::OneStop, QSerialPort::NoFlowControl);
	mySerial->OpenPort(); 
	Ctrl->init();
	
	serialThread->start();
	timerThread->start();
	controllerThread->start();
	
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::initUI()
{
	curve = new QwtPlotCurve;
	plot = new QwtPlot;
	for (int index = 0; index < plotDataSize; ++index)
	{
		xData[index] = index;
		yData[index] = index;
	}
	
	curve->setSamples(xData, yData, plotDataSize);
	curve->attach(plot);

	plot->replot();
	plot->show();
	
	ui->verticalLayout_4->addWidget(plot);
}

void MainWindow::ButtonClicked()
{
	//digitalWrite(0, HIGH);
    qDebug() << "MainWindow::ButtonClicked" << this;
	
}

void MainWindow::serialDataReceived(QString data)
{
	QVector<double> vector(0);
	vector = parseData(data);
	
	ui->textEdit->append(QString::number(vector.at(0))+"\n\r");
	qDebug() << "MainWindow::serialDataReceived" << data ;
}

void MainWindow::updateUI(QVector<double> data)
{
	ui->lineEdit->setText(QString::number(data.at(1)));
	qDebug() << "MainWindow::updateUI" << data;
	
	memmove(yData, yData + 1, (plotDataSize - 1) * sizeof(double));
	yData[plotDataSize - 1] = data.at(1);
	curve->setSamples(xData, yData, plotDataSize);
	plot->replot();
}

void MainWindow::updateSerial(QVector<double> data)
{
	ui->lineEdit->setText(QString::number(data.at(1))) ;
	bool success = mySerial->WriteToPort(QByteArray::number(27));
	qDebug() << "MainWindow::updateSerial" << "success=" << success <<
		"data.at(0)=" << QString::number(data.at(0));
}

void MainWindow::printError(QString error)
{
	qDebug() << "MainWindow::printError" << error;
}

QVector<double> MainWindow::parseData(QString data)
{
	QVector<double> vector(0);
	return vector;
}