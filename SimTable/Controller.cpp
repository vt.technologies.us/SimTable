#include "Controller.h"
#include <QString>
#include <QDebug>
#include <QtMath>
#include <wiringPi.h>
#include <QDateTime>

#define SGN(n) ( ( (n) < 0 ) ? -1 : ( (n) > 0 ) )
#define maxPWMRange 100
#define counterSensorPin 1
#define rightPWMPin 4
#define leftPWMPin 5
volatile quint64 hallCounter; 
QDateTime lastTime;
double velocity;
quint64 lastMicroTime;
double velocity_micro;
static void counterSensor();

Controller::Controller(QObject *parent)
	: QObject(parent)
{
	pid = new PID(0, 0);
	input = new double;
	output = new double;
}


Controller::~Controller()
{
	//Controller().deleteLater();
}


void Controller::init()
{
	/*
	 *	pwmSetClock(1); -> 2.342kHz
	 *	pwmSetClock(2); -> 4.81MHz
	 *	pwmSetClock(3); -> 3.19MHz
	 *	pwmSetClock(4); -> 2.398MHz
	 *	pwmSetClock(5); -> 1.919MHz
	 *	pwmSetClock(6); -> 1.6MHz
	 *	pwmSetClock(7); -> 1.3MHz
	 *	pwmSetClock(8); -> 1.2MHz
	 *	pwmSetClock(9); -> 1.067MHz
	 *	pwmSetClock(10); -> 959kHz
	 *	pwmSetClock(11); -> 871kHz
	 *	pwmSetClock(20); -> 480kHz
	 *	pwmSetClock(200); -> 48kHz
	 *	pwmSetClock(500); -> 19kHz
	 *	pwmSetClock(1000); -> 9.59kHz
	 *	pwmSetClock(2000); -> 4.802kHz
	 *	pwmSetClock(4000); -> 2.401kHz
	 *	pwmSetClock(5000); -> 10.58kHz
	 */
	pinMode(rightPWMPin, PWM_OUTPUT);
	pinMode(leftPWMPin, PWM_OUTPUT);
	//pwmSetMode(PWM_MODE_BAL);	// PWM_MODE_MS ; PWM_MODE_BAL
	//pwmSetClock(9);
	//pwmSetRange(100);
	
	hallCounter = 0;
	lastTime = QDateTime::currentDateTime(); 
	velocity = 0;
	lastMicroTime  = micros();
	pinMode(counterSensorPin, INPUT);
	int i = wiringPiISR(counterSensorPin, INT_EDGE_RISING, &counterSensor); 
	
	input[0] = 0;
	output[0] = 0;
	output[1] = 0;
	pid->setReference(10);
	pid->setCoefficient(0.8, 0.9, 0.9);

	qDebug() << "Controller::init" << this << "\n\r" << 
	"error[0]=" << pid->error << "output[0]=" << output[0];//<< "i=" << i;
}

void Controller::update()
{
	QVector<double> vector(0);
	
	output[1] = output[0];
	output[0] = plant(input, output);
	input[0] = pid->update(output[0]);

	//action(input[0]);
	
	qDebug() << "Controller::update" << this << "\n\r" << 
		"input[0]=" << input[0] << "output[0]=" << output[0] << 
		"output[1]=" << output[1] << "error[0]=" << pid->error;
		
	vector.append(input[0]);
	vector.append(output[0]);
	vector.append(output[1]);
	vector.append(pid->error);

	emit controlInfo(vector);
}

double Controller::plant(double *_input, double *_output)
{
	double a0 = 1, a1 = -0.8; 
	double b0 = 1;
	_output[0] = (1 / a0) * ( (b0*_input[0]) - (a1*_output[1]) );
	return _output[0];
}

void Controller::action(double _input)
{
	if (qFabs(_input) > maxPWMRange)
		_input = SGN(_input)*maxPWMRange;
	
	if (_input > 0)
		pwmWrite(rightPWMPin, _input);
	else
		pwmWrite(leftPWMPin, _input);
	
	qDebug() << "Controller::action" << this << "\n\r" << 
	"_input=" << _input ;
	
}

static void counterSensor()
{
	hallCounter++;
	
	QDateTime time = QDateTime::currentDateTime();
	quint64 milliSecondsDiff = lastTime.msecsTo(time);
	lastTime = time;
	velocity = 2*M_PI / milliSecondsDiff;
	
	quint64 microTime  = micros();
	quint64 microSecondsDiff = microTime - lastMicroTime;
	lastMicroTime = microTime;
	velocity_micro = 2*M_PI / microSecondsDiff;
		
	qDebug() << "static Controller::counterSensor" << "\n\r" << 
		"hallCounter=" << hallCounter <<
		"time=" << time << "milliSecondsDiff=" << milliSecondsDiff << 
		"velocity=" << velocity << 
		"microTime=" << microTime << "microSecondsDiff=" << microSecondsDiff << 
		"velocity_micro=" << velocity_micro ;
}