#ifndef Controller_H
#define Controller_H
#include <QObject>
#include "PID.h"

class Controller : public QObject
{
	Q_OBJECT
public :
	Controller(QObject *parent = 0);
	~Controller();
	
	void init();
	double plant(double *_input, double *_output);
	void action(double _input);
	
signals:
	void doSomething();
	void controlInfo(QVector<double>);
	
public slots :
    void update();
	
public:
	PID *pid;
	double *output;
	double *input;

};

#endif // Controller_H
