/****************************************************************************
** Meta object code from reading C++ file 'Serial.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Serial.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Serial.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Serial_t {
    QByteArrayData data[15];
    char stringdata[174];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Serial_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Serial_t qt_meta_stringdata_Serial = {
    {
QT_MOC_LITERAL(0, 0, 6), // "Serial"
QT_MOC_LITERAL(1, 7, 12), // "FinishedPort"
QT_MOC_LITERAL(2, 20, 0), // ""
QT_MOC_LITERAL(3, 21, 13), // "DataReadyRead"
QT_MOC_LITERAL(4, 35, 9), // "PortError"
QT_MOC_LITERAL(5, 45, 5), // "error"
QT_MOC_LITERAL(6, 51, 17), // "softResetResponse"
QT_MOC_LITERAL(7, 69, 11), // "PortProcess"
QT_MOC_LITERAL(8, 81, 12), // "ReadDataPort"
QT_MOC_LITERAL(9, 94, 7), // "timeout"
QT_MOC_LITERAL(10, 102, 14), // "serialDataSent"
QT_MOC_LITERAL(11, 117, 5), // "bytes"
QT_MOC_LITERAL(12, 123, 9), // "softReset"
QT_MOC_LITERAL(13, 133, 11), // "ErrorHandle"
QT_MOC_LITERAL(14, 145, 28) // "QSerialPort::SerialPortError"

    },
    "Serial\0FinishedPort\0\0DataReadyRead\0"
    "PortError\0error\0softResetResponse\0"
    "PortProcess\0ReadDataPort\0timeout\0"
    "serialDataSent\0bytes\0softReset\0"
    "ErrorHandle\0QSerialPort::SerialPortError"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Serial[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   64,    2, 0x06 /* Public */,
       3,    1,   65,    2, 0x06 /* Public */,
       4,    1,   68,    2, 0x06 /* Public */,
       6,    0,   71,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    0,   72,    2, 0x0a /* Public */,
       8,    0,   73,    2, 0x0a /* Public */,
       8,    1,   74,    2, 0x0a /* Public */,
      10,    1,   77,    2, 0x0a /* Public */,
      12,    0,   80,    2, 0x0a /* Public */,
      13,    1,   81,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::LongLong,   11,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 14,    5,

       0        // eod
};

void Serial::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Serial *_t = static_cast<Serial *>(_o);
        switch (_id) {
        case 0: _t->FinishedPort(); break;
        case 1: _t->DataReadyRead((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->PortError((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->softResetResponse(); break;
        case 4: _t->PortProcess(); break;
        case 5: _t->ReadDataPort(); break;
        case 6: _t->ReadDataPort((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->serialDataSent((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 8: _t->softReset(); break;
        case 9: _t->ErrorHandle((*reinterpret_cast< QSerialPort::SerialPortError(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Serial::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Serial::FinishedPort)) {
                *result = 0;
            }
        }
        {
            typedef void (Serial::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Serial::DataReadyRead)) {
                *result = 1;
            }
        }
        {
            typedef void (Serial::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Serial::PortError)) {
                *result = 2;
            }
        }
        {
            typedef void (Serial::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Serial::softResetResponse)) {
                *result = 3;
            }
        }
    }
}

const QMetaObject Serial::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Serial.data,
      qt_meta_data_Serial,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Serial::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Serial::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Serial.stringdata))
        return static_cast<void*>(const_cast< Serial*>(this));
    return QObject::qt_metacast(_clname);
}

int Serial::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void Serial::FinishedPort()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void Serial::DataReadyRead(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Serial::PortError(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Serial::softResetResponse()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
